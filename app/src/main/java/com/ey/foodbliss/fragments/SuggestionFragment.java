package com.ey.foodbliss.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.ey.foodbliss.MainActivity;
import com.ey.foodbliss.R;
import com.ey.foodbliss.listeners.SocialLoginListener;
import com.ey.foodbliss.model.Suggestion;
import com.ey.foodbliss.model.User;


public class SuggestionFragment extends Fragment implements View.OnClickListener, SocialLoginListener {

    private View rootView;
    TextInputEditText tetName, tetSuggestions;
    AppCompatButton btnSubmit;
    String name, feedback;
    private Suggestion suggestion;
    private Activity mActivity;

    public SuggestionFragment() {
        // Required empty public constructor
    }

    public static SuggestionFragment newInstance() {
        SuggestionFragment fragment = new SuggestionFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_suggestion, container, false);
        initViews(rootView);
        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        return onOptionsItemSelected(item);
    }

    private void initViews(View rootView) {
        tetName = (TextInputEditText) rootView.findViewById(R.id.tetUsername);
        tetSuggestions = (TextInputEditText) rootView.findViewById(R.id.tetPassword);
        btnSubmit = (AppCompatButton) rootView.findViewById(R.id.btnSubmit);
        btnSubmit.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.btnSubmit:
                name = tetName.getText().toString();
                feedback = tetSuggestions.getText().toString();
                suggestion = new Suggestion();
                suggestion.setName(name);
                suggestion.setFeedback(feedback);
                submitSuggestion(suggestion);
                break;
            default:
                break;
        }
    }

    private void submitSuggestion(Suggestion suggestion) {

    }

    private void login(User user) {

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
    }

    @Override
    public void onGoogleLoginComplete() {

    }

    @Override
    public void onFacebookLoginComplete(boolean success, String response) {
        if (success) {
            ((MainActivity) mActivity).setFragment(MainActivity.INDEX_HOME_FRAGMENT, null);
        } else {
            Toast.makeText(mActivity, "error", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            // Do your Work
            setHasOptionsMenu(true);
        } else {
            setHasOptionsMenu(false);
        }
    }
}
