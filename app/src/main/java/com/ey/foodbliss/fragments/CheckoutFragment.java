package com.ey.foodbliss.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.ey.foodbliss.R;
import com.ey.foodbliss.ResponseEntities.CreateOrderResponse;
import com.ey.foodbliss.ResponseEntities.LoginResponse;
import com.ey.foodbliss.adapters.CheckoutsAdapter;
import com.ey.foodbliss.database.AppDatabaseHelper;
import com.ey.foodbliss.model.CartItem;
import com.ey.foodbliss.utils.Preferences;
import com.ey.foodbliss.utils.Utils;
import com.ey.foodbliss.webservice.WebServiceListener;
import com.ey.foodbliss.webservice.WebServiceManager;

import java.util.ArrayList;


public class CheckoutFragment extends Fragment implements View.OnClickListener, WebServiceListener {

    View rootView;
    Activity mActivity;
    RecyclerView.LayoutManager rvLayoutManager;
    ArrayList<CartItem> cartItems = new ArrayList<CartItem>();
    int serviceCount = -1;
    CheckoutsAdapter checkoutsAdapter;
    RecyclerView rvCarts;
    AppCompatButton btnOrderFood;
    WebServiceManager webServiceManager;
    Preferences preferences;
    LoginResponse.Response user;
    AppDatabaseHelper appDatabaseHelper;
    RelativeLayout rlEmptyCart;

    public CheckoutFragment() {

    }

    public static CheckoutFragment newInstance() {
        CheckoutFragment fragment = new CheckoutFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        webServiceManager = new WebServiceManager(mActivity);
        preferences = new Preferences(mActivity);
        appDatabaseHelper = new AppDatabaseHelper(mActivity);
        user = preferences.getUser();
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_carts, container, false);
        rvCarts = (RecyclerView) rootView.findViewById(R.id.rvCart);
        rvCarts.setHasFixedSize(true);
        rvLayoutManager = new LinearLayoutManager(mActivity);
        rvCarts.setLayoutManager(rvLayoutManager);
        rvCarts.setItemAnimator(new DefaultItemAnimator());
        rlEmptyCart = (RelativeLayout) rootView.findViewById(R.id.rlEmptyCart);
        btnOrderFood = (AppCompatButton) rootView.findViewById(R.id.btnOrderFood);
        btnOrderFood.setOnClickListener(this);
        checkoutsAdapter = new CheckoutsAdapter(this, mActivity, cartItems);
        rvCarts.setAdapter(checkoutsAdapter);

        // Getting order list from server
        getCartItems();

        return rootView;
    }

    public void getCartItems() {
        //webServiceManager.getOrders(user.getUserId(), this);
        cartItems = appDatabaseHelper.getCartItems();
        if (isCartIsEmpty(cartItems)) {
            onCartEmpty(true);
        } else {
            btnOrderFood.setVisibility(View.VISIBLE);
            rvCarts.setVisibility(View.VISIBLE);
            rlEmptyCart.setVisibility(View.GONE);
            checkoutsAdapter = new CheckoutsAdapter(this, mActivity, cartItems);
            rvCarts.setAdapter(checkoutsAdapter);
        }
    }

    public void onCartEmpty(boolean isEmpty) {
        if (isEmpty) {
            btnOrderFood.setVisibility(View.GONE);
            rvCarts.setVisibility(View.GONE);
            rlEmptyCart.setVisibility(View.VISIBLE);
        }
    }

    public boolean isCartIsEmpty(ArrayList<CartItem> cartItems) {

        if (cartItems == null || cartItems.size() <= 0) {
            return true;
        }
        return false;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        /*inflater.inflate(R.menu.main, menu);
        super.onCreateOptionsMenu(menu, inflater);*/
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

        }
        return onOptionsItemSelected(item);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnOrderFood:
                orderFood(cartItems);
                break;
            default:
                break;
        }
    }

    private void orderFood(ArrayList<CartItem> cartItems) {
        webServiceManager.orderFood(preferences.getUser().getUser_id(), cartItems, this);
    }

    @Override
    public void onSuccess(int requestCode, int responseCode, Object response) {
        CreateOrderResponse createOrderResponse = (CreateOrderResponse) response;
        createOrderResponse.setUserId(preferences.getUser().getUser_id());
        appDatabaseHelper.addOrderToLocalDB(createOrderResponse);
        Utils.showAlert(mActivity, "Order created and your order is" + createOrderResponse.getResponse().getOrder_id());
    }

    @Override
    public void onFailure(int requestCode, int responseCode, Object response) {
        CreateOrderResponse createOrderResponse = (CreateOrderResponse) response;
        Utils.showAlert(mActivity, createOrderResponse.getMessage());
    }

    @Override
    public void isNetworkAvailable(boolean flag) {

    }
}
