package com.ey.foodbliss.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.ey.foodbliss.MainActivity;
import com.ey.foodbliss.R;
import com.ey.foodbliss.ResponseEntities.LoginResponse;
import com.ey.foodbliss.listeners.SocialLoginListener;
import com.ey.foodbliss.model.User;
import com.ey.foodbliss.utils.Preferences;
import com.ey.foodbliss.utils.Utils;
import com.ey.foodbliss.webservice.WebServiceListener;
import com.ey.foodbliss.webservice.WebServiceManager;
import com.facebook.login.Login;


public class LoginFragment extends Fragment implements View.OnClickListener, SocialLoginListener, WebServiceListener {

    private View rootView;
    TextInputEditText tetUsername, tetPassword;
    AppCompatButton btnLogin, btnGoogle, btnFacebook;
    String username, password;
    private LoginResponse.Response user;
    private Activity mActivity;
    WebServiceManager webServiceManager;
    Preferences preferences;

    public LoginFragment() {
        // Required empty public constructor
    }

    public static LoginFragment newInstance() {
        LoginFragment fragment = new LoginFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        webServiceManager = new WebServiceManager(mActivity);
        preferences = new Preferences(mActivity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_login, container, false);
        initViews(rootView);
        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.login_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_signup:
                ((MainActivity) mActivity).setFragment(MainActivity.INDEX_SIGNUP_FRAGMENT, null);
                return true;
        }
        return false;
    }

    private void initViews(View rootView) {
        tetUsername = (TextInputEditText) rootView.findViewById(R.id.tetUsername);
        tetPassword = (TextInputEditText) rootView.findViewById(R.id.tetPassword);
        btnLogin = (AppCompatButton) rootView.findViewById(R.id.btnLogin);
        btnGoogle = (AppCompatButton) rootView.findViewById(R.id.btnGoogle);
        btnFacebook = (AppCompatButton) rootView.findViewById(R.id.btnFacebook);
        btnLogin.setOnClickListener(this);
        btnGoogle.setOnClickListener(this);
        btnFacebook.setOnClickListener(this);
        initSignUpDialog();
    }

    private void initSignUpDialog() {
        AlertDialog.Builder signUpAlert = new AlertDialog.Builder(mActivity);
        signUpAlert.setMessage(mActivity.getResources().getString(R.string.signup_dialog_message));
        signUpAlert.setPositiveButton(mActivity.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                ((MainActivity) mActivity).setFragment(MainActivity.INDEX_SIGNUP_FRAGMENT, null);
            }
        });
        signUpAlert.setNegativeButton(mActivity.getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnGoogle:
                break;
            case R.id.btnFacebook:
                ((MainActivity) mActivity).loginWithFacebook(this);
                break;
            case R.id.btnLogin:
                username = tetUsername.getText().toString();
                password = tetPassword.getText().toString();
                user = new LoginResponse.Response();
                user.setUser_id(username);
                user.setPassword(password);
                if (Utils.isConnected(mActivity)) {
                    login(user);
                } else {
                    Utils.showAlert(mActivity, mActivity.getResources().getString(R.string.no_internet));
                }
                break;
            default:
                break;
        }
    }

    private void login(LoginResponse.Response user) {
        webServiceManager.login(user, this);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
    }

    @Override
    public void onGoogleLoginComplete() {

    }

    @Override
    public void onFacebookLoginComplete(boolean success, String response) {
        if (success) {
            ((MainActivity) mActivity).setFragment(MainActivity.INDEX_HOME_FRAGMENT, null);
        } else {
            Toast.makeText(mActivity, "error", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            // Do your Work
            setHasOptionsMenu(true);
        } else {
            setHasOptionsMenu(false);
        }
    }

    @Override
    public void onSuccess(int requestCode, int responseCode, Object response) {
        LoginResponse loginResponse = (LoginResponse) response;
        user = loginResponse.getUser();
        preferences.setLoginned(true);
        preferences.updateUser(user);
        ((MainActivity) mActivity).changeMenuOnLogin(true);
        moveToMenuFragment();
    }

    private void moveToMenuFragment() {
        ((MainActivity) mActivity).setFragment(MainActivity.INDEX_HOME_FRAGMENT, null);
    }

    @Override
    public void onFailure(int requestCode, int responseCode, Object response) {
        if (requestCode == WebServiceManager.REQUEST_CODE_LOGIN) {
            if (response != null) {
                LoginResponse loginResponse = (LoginResponse) response;
                Utils.showAlert(mActivity, loginResponse.getMessage());
            } else {
                Utils.showAlert(mActivity, mActivity.getResources().getString(R.string.internal_server_error));
            }
        }
    }

    private void showSignUpDialog() {

    }

    @Override
    public void isNetworkAvailable(boolean flag) {

    }
}
