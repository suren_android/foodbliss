package com.ey.foodbliss.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.ey.foodbliss.R;
import com.ey.foodbliss.ResponseEntities.LoginResponse;
import com.ey.foodbliss.ResponseEntities.OrderListResponse;
import com.ey.foodbliss.adapters.MyOrdersAdapter;
import com.ey.foodbliss.model.OrderItem;
import com.ey.foodbliss.model.Orders;
import com.ey.foodbliss.model.User;
import com.ey.foodbliss.utils.Preferences;
import com.ey.foodbliss.utils.Utils;
import com.ey.foodbliss.webservice.WebServiceListener;
import com.ey.foodbliss.webservice.WebServiceManager;

import java.util.ArrayList;
import java.util.Arrays;


public class MyOrdersFragment extends Fragment implements WebServiceListener {

    View rootView;
    Activity mActivity;
    RecyclerView.LayoutManager rvLayoutManager;
    ArrayList<Orders> orders = new ArrayList<Orders>();
    int serviceCount = -1;
    MyOrdersAdapter myOrdersAdapter;
    RecyclerView menuRecyclerView;
    WebServiceManager webServiceManager;
    Preferences preferences;
    LoginResponse.Response user;

    public MyOrdersFragment() {

    }

    public static MyOrdersFragment newInstance() {
        MyOrdersFragment fragment = new MyOrdersFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        webServiceManager = new WebServiceManager(mActivity);
        preferences = new Preferences(mActivity);
        user = preferences.getUser();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_my_orders, container, false);
        menuRecyclerView = (RecyclerView) rootView.findViewById(R.id.rvOrders);
        menuRecyclerView.setHasFixedSize(true);
        rvLayoutManager = new LinearLayoutManager(mActivity);
        menuRecyclerView.setLayoutManager(rvLayoutManager);
        menuRecyclerView.setItemAnimator(new DefaultItemAnimator());
        myOrdersAdapter = new MyOrdersAdapter(this, mActivity, orders);
        menuRecyclerView.setAdapter(myOrdersAdapter);

        // Getting order list from server
        getMyOrders();

        return rootView;
    }

    private void getMyOrders() {
        //webServiceManager.getOrders(user.getUserId(), this);
        webServiceManager.getOrders(user.getUser_id(), this);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.main, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

        }
        return onOptionsItemSelected(item);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
    }

    @Override
    public void onSuccess(int requestCode, int responseCode, Object response) {
        if (response != null) {
            OrderListResponse orderListResponse = (OrderListResponse) response;
            if (orderListResponse.getOrders() != null) {
                orders.clear();
                orders.addAll(new ArrayList<Orders>(Arrays.asList(orderListResponse.getOrders())));
                myOrdersAdapter.notifyDataSetChanged();
            } else {
                Utils.showToast(mActivity, "No Orders found");
            }
        }

    }

    @Override
    public void onFailure(int requestCode, int responseCode, Object response) {
        OrderListResponse orderListResponse = (OrderListResponse) response;
        Utils.showAlert(mActivity, orderListResponse.getMessage());
    }

    @Override
    public void isNetworkAvailable(boolean flag) {

    }

    public void cancelOrder(Orders order) {
        webServiceManager.cancelOrder(order,this);
    }
}
