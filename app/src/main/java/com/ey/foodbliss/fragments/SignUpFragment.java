package com.ey.foodbliss.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatSpinner;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.ey.foodbliss.MainActivity;
import com.ey.foodbliss.R;
import com.ey.foodbliss.ResponseEntities.LoginResponse;
import com.ey.foodbliss.ResponseEntities.SignUpResponse;
import com.ey.foodbliss.listeners.SocialLoginListener;
import com.ey.foodbliss.model.College;
import com.ey.foodbliss.model.User;
import com.ey.foodbliss.utils.Preferences;
import com.ey.foodbliss.utils.Utils;
import com.ey.foodbliss.webservice.WebServiceListener;
import com.ey.foodbliss.webservice.WebServiceManager;

import java.util.ArrayList;


public class SignUpFragment extends Fragment implements View.OnClickListener, SocialLoginListener, WebServiceListener {

    private View rootView;
    TextInputEditText tetUsername, tetPassword, tetFirstName, tetLastName, tetEmail, tetPhoneNumber;
    AppCompatButton btnSignUp, btnGoogle, btnFacebook;
    String username, password, firstName, lastName, phoneNumber, email;
    private SignUpResponse.Response user;
    private Activity mActivity;
    WebServiceManager webServiceManager;
    ArrayAdapter<String> collegeSpinnerAdapter;
    ArrayList<College> colleges = new ArrayList<College>();
    ArrayList<String> collegeNames = new ArrayList<String>();
    AppCompatSpinner collegeSpinner;
    private Preferences preferences;

    public SignUpFragment() {
        // Required empty public constructor
    }

    public static SignUpFragment newInstance() {
        SignUpFragment fragment = new SignUpFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        webServiceManager = new WebServiceManager(mActivity);
        preferences = new Preferences(mActivity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_signup, container, false);
        initViews(rootView);
        getColleges();
        return rootView;
    }

    private void getColleges() {
        //webServiceManager.getColleges(this);
        collegeNames.add("fdff");
        collegeNames.add("fdfd");
        collegeNames.add("fdfd");
        collegeNames.add("fdfdf");
        collegeNames.add("dffdf");
        collegeNames.add("fdfdfd");
        collegeNames.add("fdfdf");
        collegeNames.add("fddfdf");
        collegeNames.add("fdfdf");
        collegeNames.add("fdfdfd");
        collegeNames.add("fdfdfd");
        collegeNames.add("fdfdfd");

        collegeSpinnerAdapter.notifyDataSetChanged();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.signup_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_signup:
                ((MainActivity) mActivity).onBackPressed();
                return true;
        }
        return onOptionsItemSelected(item);
    }

    private void initViews(View rootView) {
        tetUsername = (TextInputEditText) rootView.findViewById(R.id.terUsername);
        tetPassword = (TextInputEditText) rootView.findViewById(R.id.tetPassword);
        tetFirstName = (TextInputEditText) rootView.findViewById(R.id.tetFirstName);
        tetLastName = (TextInputEditText) rootView.findViewById(R.id.tetLastname);
        tetEmail = (TextInputEditText) rootView.findViewById(R.id.tetEmail);
        tetPhoneNumber = (TextInputEditText) rootView.findViewById(R.id.tetPhoneNumber);
        btnSignUp = (AppCompatButton) rootView.findViewById(R.id.btnSignUp);
        btnGoogle = (AppCompatButton) rootView.findViewById(R.id.btnGoogle);
        btnFacebook = (AppCompatButton) rootView.findViewById(R.id.btnFacebook);
        collegeSpinner = (AppCompatSpinner) rootView.findViewById(R.id.spColleges);
        btnSignUp.setOnClickListener(this);
        btnGoogle.setOnClickListener(this);
        btnFacebook.setOnClickListener(this);
        collegeSpinner = (AppCompatSpinner) rootView.findViewById(R.id.spColleges);
        collegeSpinnerAdapter = new ArrayAdapter<String>(mActivity, R.layout.college_item, collegeNames);
        collegeSpinnerAdapter.setDropDownViewResource(R.layout.college_item);
        collegeSpinner.setAdapter(collegeSpinnerAdapter);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnGoogle:
                break;
            case R.id.btnFacebook:
                ((MainActivity) mActivity).loginWithFacebook(this);
                break;
            case R.id.btnSignUp:
                user = getUserDataFromInput();
                if (Utils.isConnected(mActivity)) {
                    signUp(user);
                } else {
                    Utils.showAlert(mActivity, mActivity.getResources().getString(R.string.no_internet));
                }
                break;
            default:
                break;
        }
    }

    private SignUpResponse.Response getUserDataFromInput() {
        username = tetUsername.getText().toString();
        password = tetPassword.getText().toString();
        firstName = tetFirstName.getText().toString();
        lastName = tetLastName.getText().toString();
        phoneNumber = tetPhoneNumber.getText().toString();
        email = tetEmail.getText().toString();
        user = new SignUpResponse.Response();
        user.setUser_id(username);
        user.setPassword(password);
        user.setFirstname(firstName);
        user.setLastname(lastName);
        user.setEmail(email);
        user.setPhone_number(phoneNumber);
        return user;
    }

    private void signUp(SignUpResponse.Response user) {
        webServiceManager.signUp(user, this);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
    }

    @Override
    public void onGoogleLoginComplete() {

    }

    @Override
    public void onFacebookLoginComplete(boolean success, String response) {
        if (success) {
            ((MainActivity) mActivity).setFragment(MainActivity.INDEX_HOME_FRAGMENT, null);
        } else {
            Toast.makeText(mActivity, "error", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onSuccess(int requestCode, int responseCode, Object response) {
        if (requestCode == WebServiceManager.REQUEST_CODE_SIGNUP) {
            login(user);
        } else if (requestCode == WebServiceManager.REQUEST_CODE_LOGIN) {
            preferences.updateUser(user);
            ((MainActivity) mActivity).changeMenuOnLogin(true);
            moveToMenuFragment();
        }
    }

    private void login(SignUpResponse.Response user) {
        LoginResponse.Response loginUser = new LoginResponse.Response();
        loginUser.setEmail(user.getEmail());
        loginUser.setUser_id(user.getEmail());
        loginUser.setPassword(user.getPassword());
        webServiceManager.login(loginUser, this);
    }

    @Override
    public void onFailure(int requestCode, int responseCode, Object response) {
        if (response != null) {
            if (requestCode == WebServiceManager.REQUEST_CODE_SIGNUP) {
                SignUpResponse signUpResponse = (SignUpResponse) response;
                Utils.showAlert(mActivity, signUpResponse.getMessage());
            } else if (requestCode == WebServiceManager.REQUEST_CODE_LOGIN) {
                LoginResponse loginResponse = (LoginResponse) response;
                Utils.showAlert(mActivity, loginResponse.getMessage());
            }
        } else {
            Utils.showAlert(mActivity, mActivity.getResources().getString(R.string.internal_server_error));
        }

    }

    @Override
    public void isNetworkAvailable(boolean flag) {

    }

    private void moveToMenuFragment() {
        ((MainActivity) mActivity).setFragment(MainActivity.INDEX_HOME_FRAGMENT, null);
    }
}
