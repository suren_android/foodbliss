package com.ey.foodbliss.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.ey.foodbliss.MainActivity;
import com.ey.foodbliss.R;
import com.ey.foodbliss.ResponseEntities.CategoryResponse;
import com.ey.foodbliss.ResponseEntities.FoodListResponse;
import com.ey.foodbliss.activities.FoodDetailActivity;
import com.ey.foodbliss.adapters.CategoryAdapter;
import com.ey.foodbliss.model.Category;
import com.ey.foodbliss.model.FoodItem;
import com.ey.foodbliss.utils.Utils;
import com.ey.foodbliss.webservice.WebServiceListener;
import com.ey.foodbliss.webservice.WebServiceManager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;


public class MenuFragment extends Fragment implements WebServiceListener {

    private View rootView;
    private Activity mActivity;
    private GridLayoutManager lLayout;
    HashMap<Integer, ArrayList<FoodItem>> totalFoodItems = new HashMap<>();
    ArrayList<FoodItem> foodItems = new ArrayList<FoodItem>();
    int serviceCount = -1;
    private CategoryAdapter categoryAdapter;
    private RecyclerView menuRecyclerView;
    WebServiceManager webServiceManager;
    private CategoryResponse categoryResponse;
    ArrayList<CategoryResponse.Response> categories = new ArrayList<>();
    boolean isCategoryLoaded = false;

    public MenuFragment() {
        // Required empty public constructor
    }

    public static MenuFragment newInstance() {
        MenuFragment fragment = new MenuFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        webServiceManager = new WebServiceManager(mActivity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_menu, container, false);
        menuRecyclerView = (RecyclerView) rootView.findViewById(R.id.rvFoodMenu);
        menuRecyclerView.setHasFixedSize(true);

        getCategories();

        lLayout = new GridLayoutManager(mActivity, 3);
        menuRecyclerView.setLayoutManager(lLayout);
        categoryAdapter = new CategoryAdapter(this, mActivity, categories);
        menuRecyclerView.setAdapter(categoryAdapter);

        return rootView;
    }

    /*@Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.main, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

        }
        return false;
    }*/

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
    }

    public void getCategories() {
        if (Utils.isConnected(mActivity)) {
            webServiceManager.getCategories(this);
        } else {
            Utils.showAlert(mActivity, mActivity.getResources().getString(R.string.no_internet));
        }
    }

    public void loadFoodsFromCategory(String name) {
        menuRecyclerView.setVisibility(View.GONE);
        getCategories();
        menuRecyclerView.setVisibility(View.VISIBLE);
    }

    public void moveToFoodDetailActivity(FoodItem foodItem) {
        Intent intent = new Intent(mActivity, FoodDetailActivity.class);
        startActivity(intent);
    }

    public void moveToFoodListFragment(CategoryResponse.Response category) {
        ((MainActivity) mActivity).setFragment(MainActivity.INDEX_FOOD_LIST_FRAGMENT, category.getCategory_id());
    }

    public void moveMenuOneStepBack() {
        if (serviceCount > 0) {
            totalFoodItems.remove(serviceCount);
            serviceCount--;
            foodItems = totalFoodItems.get(serviceCount);
            categoryAdapter.notifyDataSetChanged();
            Toast.makeText(mActivity, "" + serviceCount + ":" + foodItems.size(), Toast.LENGTH_LONG).show();
        } else {
            mActivity.finish();
        }
    }

    @Override
    public void onSuccess(int requestCode, int responseCode, Object response) {
        categoryResponse = (CategoryResponse) response;
        categories.addAll(new ArrayList<>(Arrays.asList(categoryResponse.getCategories())));
        categoryAdapter.notifyDataSetChanged();
        isCategoryLoaded = true;
    }

    @Override
    public void onFailure(int requestCode, int responseCode, Object response) {

    }

    @Override
    public void isNetworkAvailable(boolean flag) {

    }
    // TODO: Rename method, update argument and hook method into UI event


}
