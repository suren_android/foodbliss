package com.ey.foodbliss.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.ey.foodbliss.R;
import com.ey.foodbliss.ResponseEntities.FoodListResponse;
import com.ey.foodbliss.activities.FoodDetailActivity;
import com.ey.foodbliss.adapters.FoodListAdapter;
import com.ey.foodbliss.model.Foods;
import com.ey.foodbliss.model.FoodItem;
import com.ey.foodbliss.webservice.WebServiceListener;
import com.ey.foodbliss.webservice.WebServiceManager;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;


public class FoodListFragment extends Fragment implements WebServiceListener {

    private View rootView;
    private Activity mActivity;
    private GridLayoutManager lLayout;
    HashMap<Integer, ArrayList<FoodItem>> totalFoodItems = new HashMap<>();
    ArrayList<FoodItem> foodItems = new ArrayList<FoodItem>();
    int serviceCount = -1;
    private FoodListAdapter foodListAdapter;
    private RecyclerView menuRecyclerView;
    WebServiceManager webServiceManager;
    private FoodListResponse foodListResponse;
    ArrayList<Foods> foods = new ArrayList<>();
    private String categoryId = "";

    public FoodListFragment() {
        // Required empty public constructor
    }

    public static FoodListFragment newInstance(String categoryId) {
        FoodListFragment fragment = new FoodListFragment();
        fragment.setCategoyId(categoryId);
        return fragment;
    }

    private void setCategoyId(String categoryId) {
        this.categoryId = categoryId;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        webServiceManager = new WebServiceManager(mActivity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_food_list, container, false);
        menuRecyclerView = (RecyclerView) rootView.findViewById(R.id.rvFoodMenu);
        menuRecyclerView.setHasFixedSize(true);

        getFoods(categoryId);

        lLayout = new GridLayoutManager(mActivity, 3);
        menuRecyclerView.setLayoutManager(lLayout);
        foodListAdapter = new FoodListAdapter(this, mActivity, foods);
        menuRecyclerView.setAdapter(foodListAdapter);

        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.main, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

        }
        return onOptionsItemSelected(item);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
    }

    public void getFoods(String categoryId) {
        webServiceManager.getFoods(categoryId, this);
    }

    public void loadFoodsFromCategory(String name) {
        menuRecyclerView.setVisibility(View.GONE);
        getFoods(categoryId);
        menuRecyclerView.setVisibility(View.VISIBLE);
    }

    public void moveToFoodDetailActivity(Foods foodItem) {
        Intent intent = new Intent(mActivity, FoodDetailActivity.class);
        intent.putExtra("food", (Serializable) foodItem);
        startActivity(intent);
    }

    public void moveMenuOneStepBack() {
        if (serviceCount > 0) {
            totalFoodItems.remove(serviceCount);
            serviceCount--;
            foodItems = totalFoodItems.get(serviceCount);
            foodListAdapter.notifyDataSetChanged();
            Toast.makeText(mActivity, "" + serviceCount + ":" + foodItems.size(), Toast.LENGTH_LONG).show();
        } else {
            mActivity.finish();
        }
    }

    @Override
    public void onSuccess(int requestCode, int responseCode, Object response) {
        foodListResponse = (FoodListResponse) response;
        foods.addAll(new ArrayList<>(Arrays.asList(foodListResponse.getFoods())));
        foodListAdapter.notifyDataSetChanged();
    }

    @Override
    public void onFailure(int requestCode, int responseCode, Object response) {

    }

    @Override
    public void isNetworkAvailable(boolean flag) {

    }
    // TODO: Rename method, update argument and hook method into UI event


}
