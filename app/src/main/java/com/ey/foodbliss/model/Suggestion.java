package com.ey.foodbliss.model;

/**
 * Created by Surendar.V on 10/6/2017.
 */

public class Suggestion {
    String name, feedback;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }
}
