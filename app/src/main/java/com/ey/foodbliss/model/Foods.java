package com.ey.foodbliss.model;

import java.io.Serializable;

/**
 * Created by Surendar.V on 10/14/2017.
 */

public class Foods implements Serializable {
    private String price;

    private String contents;

    private String name;

    private String category_id;

    private String food_id;

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getContents() {
        return contents;
    }

    public void setContents(String contents) {
        this.contents = contents;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getFood_id() {
        return food_id;
    }

    public void setFood_id(String food_id) {
        this.food_id = food_id;
    }

    @Override
    public String toString() {
        return "ClassPojo [price = " + price + ", contents = " + contents + ", name = " + name + ", category_id = " + category_id + ", food_id = " + food_id + "]";
    }
}