package com.ey.foodbliss.model;

public class Orders {
    private String amount;

    private String status;

    private String order_items;

    private String user_id;

    private String order_date;

    private String order_id;

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getOrder_items() {
        return order_items;
    }

    public void setOrder_items(String order_items) {
        this.order_items = order_items;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getOrder_date() {
        return order_date;
    }

    public void setOrder_date(String order_date) {
        this.order_date = order_date;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    @Override
    public String toString() {
        return "ClassPojo [amount = " + amount + ", status = " + status + ", order_items = " + order_items + ", user_id = " + user_id + ", order_date = " + order_date + ", order_id = " + order_id + "]";
    }
}
