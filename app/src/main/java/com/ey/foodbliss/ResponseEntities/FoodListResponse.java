package com.ey.foodbliss.ResponseEntities;

import com.ey.foodbliss.model.Foods;

/**
 * Created by Surendar.V on 10/12/2017.
 */


public class FoodListResponse {
    private Foods[] foods;
    private String message;

    public Foods[] getFoods() {
        return foods;
    }

    public void setFoods(Foods[] foods) {
        this.foods = foods;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "ClassPojo [foods = " + foods + "]";
    }
}

