package com.ey.foodbliss.ResponseEntities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SignUpResponse {
    @SerializedName("response")
    @Expose
    private Response user;

    private String status, message;

    public Response getUser() {
        return user;
    }

    public void setUser(Response user) {
        this.user = user;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "ClassPojo [user = " + user + ", status = " + status + "]";
    }

    public static class Response {
        private String phone_number;

        private String date_added;

        private String email;

        private String lastname;

        private String device_id;

        private String user_id;

        private String firstname;

        private String password;

        private String login_type;

        public Response(String phone_number, String date_added, String email, String lastname, String device_id, String user_id, String firstname, String password, String login_type) {
            this.phone_number = phone_number;
            this.date_added = date_added;
            this.email = email;
            this.lastname = lastname;
            this.device_id = device_id;
            this.user_id = user_id;
            this.firstname = firstname;
            this.password = password;
            this.login_type = login_type;
        }

        public Response() {

        }

        public String getPhone_number() {
            return phone_number;
        }

        public void setPhone_number(String phone_number) {
            this.phone_number = phone_number;
        }

        public String getDate_added() {
            return date_added;
        }

        public void setDate_added(String date_added) {
            this.date_added = date_added;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getLastname() {
            return lastname;
        }

        public void setLastname(String lastname) {
            this.lastname = lastname;
        }

        public String getDevice_id() {
            return device_id;
        }

        public void setDevice_id(String device_id) {
            this.device_id = device_id;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getFirstname() {
            return firstname;
        }

        public void setFirstname(String firstname) {
            this.firstname = firstname;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getLogin_type() {
            return login_type;
        }

        public void setLogin_type(String login_type) {
            this.login_type = login_type;
        }


        @Override
        public String toString() {
            return "ClassPojo [phone_number = " + phone_number + ", date_added = " + date_added + ", email = " + email + ", lastname = " + lastname + ", device_id = " + device_id + ", user_id = " + user_id + ", firstname = " + firstname + ", password = " + password + ", login_type = " + login_type + "]";
        }
    }

}