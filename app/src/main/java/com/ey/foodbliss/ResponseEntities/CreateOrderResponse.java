package com.ey.foodbliss.ResponseEntities;

/**
 * Created by Surendar.V on 10/27/2017.
 */

public class CreateOrderResponse {
    public Response response;

    public String status, message, userId;

    public Response getResponse() {
        return response;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setResponse(Response response) {
        this.response = response;
    }

    public String getStatus() {
        return status;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "ClassPojo [response = " + response + ", status = " + status + "]";
    }


    public class Response {
        public String orders;

        public String order_id;

        public String getOrders() {
            return orders;
        }

        public void setOrders(String orders) {
            this.orders = orders;
        }

        public String getOrder_id() {
            return order_id;
        }

        public void setOrder_id(String order_id) {
            this.order_id = order_id;
        }

        @Override
        public String toString() {
            return "ClassPojo [orders = " + orders + ", order_id = " + order_id + "]";
        }
    }
}

