package com.ey.foodbliss.ResponseEntities;

import com.ey.foodbliss.model.Orders;

/**
 * Created by Surendar.V on 10/12/2017.
 */


public class OrderListResponse {
    private Orders[] orders;
    String message;

    public Orders[] getOrders() {
        return orders;
    }

    public void setOrders(Orders[] orders) {
        this.orders = orders;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "ClassPojo [orders = " + orders + "]";
    }
}
