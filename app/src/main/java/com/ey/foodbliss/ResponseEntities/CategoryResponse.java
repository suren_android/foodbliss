package com.ey.foodbliss.ResponseEntities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Surendar.V on 10/12/2017.
 */

public class CategoryResponse {

    private Response[] categories;

    public Response[] getCategories() {
        return categories;
    }

    public void setCategories(Response[] categories) {
        this.categories = categories;
    }

    @Override
    public String toString() {
        return "ClassPojo [categories = " + categories + "]";
    }

    public class Response {
        private String name;

        private String category_id;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getCategory_id() {
            return category_id;
        }

        public void setCategory_id(String category_id) {
            this.category_id = category_id;
        }

        @Override
        public String toString() {
            return "ClassPojo [name = " + name + ", category_id = " + category_id + "]";
        }
    }
}