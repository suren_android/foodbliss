package com.ey.foodbliss.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.ey.foodbliss.ResponseEntities.LoginResponse;
import com.ey.foodbliss.ResponseEntities.SignUpResponse;
import com.ey.foodbliss.model.User;
import com.facebook.login.Login;


/**
 * Created by user on 9/10/2015.
 */
public class Preferences {

    Context context;
    SharedPreferences preferences;
    public static String USER_ID = "user_id", PASSWORD = "password", EMAIL = "email", PHONE_NUMBER = "phone_number", FIRSTNAME = "firstname", LASTNAME = "lastname", LOGIN_TYPE = "login_type";

    boolean isRemembered, isRegistered, isLoginned;

    public Preferences(Context context) {

        this.context = context;
        preferences = context.getSharedPreferences("prefs", 0);
    }


    public boolean isRegistered() {
        isRegistered = preferences.getBoolean("isRegistered", false);

        return isRegistered;
    }

    public void setRegistered(boolean isRegistered) {
        preferences.edit().putBoolean("isRegistered", isRegistered).commit();
        this.isRegistered = isRegistered;
    }


    public boolean isLoginned() {
        isLoginned = preferences.getBoolean("isLoginned", false);
        return isLoginned;
    }

    public void setLoginned(boolean isLoginned) {
        preferences.edit().putBoolean("isLoginned", isLoginned).commit();
        this.isLoginned = isLoginned;
    }

    public void updateUser(LoginResponse.Response user) {

        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(USER_ID, user.getUser_id());
        editor.putString(EMAIL, user.getPassword());
        editor.putString(PASSWORD, user.getPassword());
        editor.putString(FIRSTNAME, user.getPassword());
        editor.putString(LASTNAME, user.getPassword());
        editor.putString(PHONE_NUMBER, user.getPassword());
        editor.putString(LOGIN_TYPE, user.getLogin_type());
        editor.commit();

    }

    public void updateUser(SignUpResponse.Response user) {

        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(USER_ID, user.getUser_id());
        editor.putString(EMAIL, user.getPassword());
        editor.putString(PASSWORD, user.getPassword());
        editor.putString(FIRSTNAME, user.getPassword());
        editor.putString(LASTNAME, user.getPassword());
        editor.putString(PHONE_NUMBER, user.getPassword());
        editor.putString(LOGIN_TYPE, user.getLogin_type());
        editor.commit();

    }

    public LoginResponse.Response getUser() {
        LoginResponse.Response user = new LoginResponse.Response();
        user.setUser_id(preferences.getString(USER_ID, ""));
        user.setPassword(preferences.getString(PASSWORD, ""));
        user.setEmail(preferences.getString(EMAIL, ""));
        user.setFirstname(preferences.getString(FIRSTNAME, ""));
        user.setFirstname(preferences.getString(FIRSTNAME, ""));
        user.setLastname(preferences.getString(LASTNAME, ""));
        user.setLogin_type(preferences.getString(LOGIN_TYPE, ""));
        user.setPhone_number(preferences.getString(PHONE_NUMBER, ""));
        return user;
    }

    public void clearAll() {
        preferences.edit().clear().commit();
    }
}
