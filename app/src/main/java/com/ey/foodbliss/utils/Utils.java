package com.ey.foodbliss.utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatEditText;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;


import com.ey.foodbliss.AppController;

import java.io.File;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by Surendar.V on 12-05-2017.
 */

public class Utils {

    public static void showAlert(Activity mActivity, String s) {
        AlertDialog.Builder invalidUserDialog = new AlertDialog.Builder(mActivity);
        invalidUserDialog.setTitle("Message");
        invalidUserDialog.setMessage(s);
        invalidUserDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        invalidUserDialog.show();
    }

    public static void showAlert(Context mActivity, String s) {
        AlertDialog.Builder invalidUserDialog = new AlertDialog.Builder(mActivity);
        invalidUserDialog.setTitle("Message");
        if (s.contains("dn not unique based on rules")) {
            s = "A user with that email account already exists";
        }
        invalidUserDialog.setMessage(s);
        invalidUserDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        invalidUserDialog.show();
    }

    public static boolean isConnected(Activity _activity) {
        ConnectivityManager conMgr = (ConnectivityManager) _activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (conMgr.getActiveNetworkInfo() != null
                && conMgr.getActiveNetworkInfo().isAvailable()
                && conMgr.getActiveNetworkInfo().isConnected())
            return true;
        else
            return false;
    }//checkInternetConnection()

    public static ProgressDialog showProgress(String msg) {
        ProgressDialog progressDialog = new ProgressDialog(AppController.getInstance().getBaseContext());
        progressDialog.setMessage(msg);
        return progressDialog;
    }

    public static void showToast(Context context, String s) {
        Toast.makeText(context, s, Toast.LENGTH_LONG).show();
    }

    public static String getFacebokKeyHash(Context context) {

        String keyHash = null;
        try {
            PackageInfo info = context.getPackageManager().getPackageInfo(
                    context.getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());

                keyHash = Base64.encodeToString(md.digest(), Base64.DEFAULT);
                Log.i(" Facebook KeyHash:",
                        Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            Log.e("", "Exception(NameNotFoundException) : " + e);

        } catch (NoSuchAlgorithmException e) {
            Log.e("", "Exception(NoSuchAlgorithmException) : " + e);
        }
        return keyHash;

    }

}
