package com.ey.foodbliss.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.ey.foodbliss.ResponseEntities.CreateOrderResponse;
import com.ey.foodbliss.model.CartItem;
import com.ey.foodbliss.model.FoodItem;
import com.ey.foodbliss.model.Foods;

import java.util.ArrayList;

/**
 * Created by suren on 7/5/2016.
 */
public class AppDatabaseHelper extends SQLiteOpenHelper {

    Context mContext;

    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "FoodBlissDBManager";

    // Contacts table name
    private static final String TABLE_CATEGORY = "categories";
    private static final String TABLE_CART = "cart";
    private static final String TABLE_ORDERS = "orders";

    //  Category Table Columns names
    private static final String CATEGORY_ID = "category_id";
    private static final String CATEGORY_NAME = "category_name";

    // Cart Table Columns names
    private static final String USER_ID = "user_id";
    private static final String CART_ID = "cartId";
    private static final String FOOD_ID = "foodId";
    private static final String FOOD_NAME = "foodName";
    private static final String PRICE = "foodPrice";
    private static final String QUANTITY = "quantity";
    private static final String SUB_AMOUNT = "aubAmount";

    private static final String ORDER_STATUS = "order_status";
    private static final String ORDER_ITEMS = "order_items";
    private static final String ORDER_ID = "order_id";

    public AppDatabaseHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, DATABASE_NAME, factory, DATABASE_VERSION);
        mContext = context;
    }

    public AppDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        mContext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        execCreateTableQuery(db);
        execCartTableQuery(db);
        execOrderTableQuery(db);

    }

    private void execOrderTableQuery(SQLiteDatabase db) {
        String CREATE_ORDER_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_ORDERS + "("
                + USER_ID + " TEXT,"
                + ORDER_ID + " TEXT,"
                + ORDER_ITEMS + " TEXT,"
                + ORDER_STATUS + " TEXT"
                + ")";

        db.execSQL(CREATE_ORDER_TABLE);
    }

    private void execCreateTableQuery(SQLiteDatabase db) {
        String CREATE_CONTACTS_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_CATEGORY + "("
                + CATEGORY_ID + " INTEGER PRIMARY KEY,"
                + CATEGORY_NAME + " TEXT"
                + ")";
        db.execSQL(CREATE_CONTACTS_TABLE);
    }

    private void execCartTableQuery(SQLiteDatabase db) {
        String CREATE_CART_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_CART + "("
                + USER_ID + " TEXT,"
                + CART_ID + " TEXT,"
                + FOOD_ID + " TEXT,"
                + FOOD_NAME + " TEXT,"
                + PRICE + " TEXT,"
                + QUANTITY + " TEXT,"
                + SUB_AMOUNT + " TEXT"
                + ")";

        db.execSQL(CREATE_CART_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CATEGORY);
        // Create tables again
        onCreate(db);
    }

    public long addCategories(ArrayList<FoodItem> foodItems) {

        long result = 0;
        for (FoodItem foodItem : foodItems) {

            try {
                SQLiteDatabase db = this.getWritableDatabase();

                ContentValues values = new ContentValues();
                values.put(CATEGORY_ID, foodItem.getItemId());
                values.put(CATEGORY_NAME, foodItem.getName());
                // Inserting Row
                result = db.insert(TABLE_CATEGORY, null, values);
                db.close(); // Closing database connection
                Log.d("add food category", "adding category " + foodItem.getName());
            } catch (SQLiteException e) {
                Log.e("addRouteToLocalDB", e.getMessage());
            }
        }

        return result;
    }

    public ArrayList<FoodItem> getCategories() {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<FoodItem> stops = new ArrayList<FoodItem>();

        Cursor cursor = db.query(TABLE_CATEGORY, null, null,
                null,
                null, null, null, null);

        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    FoodItem foodItem = new FoodItem();
                    String categoryId = cursor.getString(cursor.getColumnIndex(CATEGORY_ID));
                    foodItem.setItemId(categoryId);
                    String categoryName = cursor.getString(cursor.getColumnIndex(CATEGORY_NAME));
                    foodItem.setName(categoryName);
                    stops.add(foodItem);
                } while (cursor.moveToNext());
            }
        }
        return stops;
    }

    public long addToCart(CartItem cart) {

        long result = 0;

        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(USER_ID, cart.getUserId());
            values.put(CART_ID, cart.getCartId());
            values.put(FOOD_ID, cart.getId());
            values.put(FOOD_NAME, cart.getName());
            values.put(PRICE, cart.getPrice());
            values.put(QUANTITY, cart.getQuantity());
            values.put(SUB_AMOUNT, cart.getSubAmount());
            // Inserting Row
            result = db.insert(TABLE_CART, null, values);
            db.close(); // Closing database connection
        } catch (SQLiteException e) {
            Log.e("addToCart", e.getMessage());
        }

        return result;
    }

    public long removeFromCart(CartItem cart) {

        long result = 0;

        try {
            SQLiteDatabase db = this.getWritableDatabase();
            String whereClause = CART_ID + "=?";
            // Deleting Row
            result = db.delete(TABLE_CART, whereClause, new String[]{cart.getCartId()});
            db.close(); // Closing database connection
        } catch (SQLiteException e) {
            Log.e("removeFromCart", e.getMessage());
        }

        return result;
    }

    public ArrayList<CartItem> getCartItems() {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<CartItem> cartItems = new ArrayList<CartItem>();

        Cursor cursor = db.query(TABLE_CART, null, null,
                null,
                null, null, null, null);

        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    CartItem cartItem = new CartItem();
                    String userId = cursor.getString(cursor.getColumnIndex(USER_ID));
                    cartItem.setUserId(userId);
                    String cartId = cursor.getString(cursor.getColumnIndex(CART_ID));
                    cartItem.setCartId(cartId);
                    String foodId = cursor.getString(cursor.getColumnIndex(FOOD_ID));
                    cartItem.setId(foodId);
                    String foodName = cursor.getString(cursor.getColumnIndex(FOOD_NAME));
                    cartItem.setName(foodName);
                    String foodPrice = cursor.getString(cursor.getColumnIndex(PRICE));
                    cartItem.setPrice(foodPrice);
                    String quantity = cursor.getString(cursor.getColumnIndex(QUANTITY));
                    cartItem.setQuantity(quantity);
                    String subAmount = cursor.getString(cursor.getColumnIndex(SUB_AMOUNT));
                    cartItem.setSubAmount(subAmount);
                    cartItems.add(cartItem);
                } while (cursor.moveToNext());
            }
        }
        return cartItems;
    }

    public long addOrderToLocalDB(CreateOrderResponse createOrderResponse) {

        long result = 0;

        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(USER_ID, createOrderResponse.getUserId());
            values.put(ORDER_STATUS, createOrderResponse.getStatus());
            values.put(ORDER_ID, createOrderResponse.getResponse().getOrder_id());
            values.put(ORDER_ITEMS, createOrderResponse.getResponse().getOrders());
            // Inserting Row
            result = db.insert(TABLE_CART, null, values);
            db.close(); // Closing database connection
        } catch (SQLiteException e) {
            Log.e("addOrderToLocalDB", e.getMessage());
        }

        return result;
    }
}
