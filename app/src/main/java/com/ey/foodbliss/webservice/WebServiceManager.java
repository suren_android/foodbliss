package com.ey.foodbliss.webservice;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.ey.foodbliss.AppController;
import com.ey.foodbliss.R;
import com.ey.foodbliss.ResponseEntities.CategoryResponse;
import com.ey.foodbliss.ResponseEntities.CreateOrderResponse;
import com.ey.foodbliss.ResponseEntities.FoodListResponse;
import com.ey.foodbliss.ResponseEntities.LoginResponse;
import com.ey.foodbliss.ResponseEntities.OrderListResponse;
import com.ey.foodbliss.ResponseEntities.SignUpResponse;
import com.ey.foodbliss.fragments.CheckoutFragment;
import com.ey.foodbliss.fragments.MyOrdersFragment;
import com.ey.foodbliss.model.CartItem;
import com.ey.foodbliss.model.Orders;
import com.ey.foodbliss.model.User;
import com.ey.foodbliss.utils.Utils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class WebServiceManager {

    Context context;
    /*Request codes*/
    public static int REQUEST_CODE_LOGIN = 1000;
    public static int REQUEST_CODE_SIGNUP = 1001;
    public static int REQUEST_CODE_GET_COLLEGES = 1002;
    public static int REQUEST_CODE_GET_CATEGORIES = 1003;
    public static int REQUEST_CODE_GET_FOODS = 1004;
    public static int REQUEST_CODE_GET_MY_ORDERS = 1005;
    public static int REQUEST_CODE_CANCEL_ORDER = 1006;
    final ProgressDialog pDialog;

    /*URLs*/
    private static final String URL_LOGIN = "http://18.221.142.208/cliceat/user/login?";
    private static final String URL_SIGNUP = "http://18.221.142.208/cliceat/user/register?";
    private static final String URL_GET_CATEGORIES = "http://18.221.142.208/cliceat/categories/getcategories";
    private static final String URL_GET_FOODS = "http://18.221.142.208/cliceat/categories/getfoods?category_id=";
    private static final String URL_MY_ORDERS = "http://18.221.142.208/cliceat/user/getorders?user_id=";
    private static final String URL_ORDER_FOOD = "http://18.221.142.208/cliceat/user/createOrder";
    private static final String URL_CANCEL_ORDER = "http://18.221.142.208/cliceat/user/cancelOrder";

    public WebServiceManager(Context context) {
        this.context = context;
        pDialog = new ProgressDialog(this.context);
        //pDialog.setCancelable(false);
    }

    public void login(final LoginResponse.Response user, final WebServiceListener webServiceListener) {

        // Tag used to cancel the request
        String tag_json_obj = "login";
        String url = URL_LOGIN + "email=" + user.getUser_id() + "&password=" + user.getPassword();

        //final ProgressDialog pDialog = new ProgressDialog(context);
        pDialog.setMessage("Logging in...");
        pDialog.show();


        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                url, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("", response.toString());
                        pDialog.hide();
                        LoginResponse loginResponse = new Gson().fromJson(response.toString(), LoginResponse.class);
                        if (loginResponse.getStatus() != null && loginResponse.getStatus().equalsIgnoreCase("success")) {
                            webServiceListener.onSuccess(REQUEST_CODE_LOGIN, 0, loginResponse);
                        } else {
                            webServiceListener.onFailure(REQUEST_CODE_LOGIN, 0, loginResponse);
                        }

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.hide();
                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        LoginResponse loginResponse = new Gson().fromJson(res, LoginResponse.class);
                        webServiceListener.onFailure(REQUEST_CODE_LOGIN, 0, loginResponse);

                    } catch (UnsupportedEncodingException e1) {
                        e1.printStackTrace();
                        webServiceListener.onFailure(REQUEST_CODE_LOGIN, 0, null);
                    }
                } else {
                    webServiceListener.onFailure(REQUEST_CODE_LOGIN, 0, null);
                }
            }
        }) {

        };

        // Adding request to request queue
        // jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(2000,
                3,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        if (Utils.isConnected((Activity) context)) {
            AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
        } else {
            webServiceListener.isNetworkAvailable(false);
        }

    }

    public void signUp(final SignUpResponse.Response user, final WebServiceListener webServiceListener) {

        // Tag used to cancel the request
        String tag_json_obj = "signup";
        String url = URL_SIGNUP + "firstname=" + user.getFirstname() + "&lastname=" + user.getLastname() + "&email=" + user.getEmail() +
                "&password=" + user.getPassword() + "&phone_number=" + user.getPhone_number() + "&login_type=default&device_id=hsdjakdhsja";

        //final ProgressDialog pDialog = new ProgressDialog(context);
        pDialog.setMessage("Registering...");
        pDialog.show();


        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                url, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("", response.toString());
                        pDialog.hide();
                        SignUpResponse signUpResponse = new Gson().fromJson(response.toString(), SignUpResponse.class);
                        if (signUpResponse != null && signUpResponse.getStatus() != null && signUpResponse.getStatus().equalsIgnoreCase("success")) {
                            webServiceListener.onSuccess(REQUEST_CODE_SIGNUP, 200, signUpResponse);
                        } else {
                            webServiceListener.onFailure(REQUEST_CODE_SIGNUP, 200, signUpResponse);
                        }

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.hide();
                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers, "utf-8"));


                    } catch (UnsupportedEncodingException e1) {
                        e1.printStackTrace();
                    }
                } else {

                }
            }
        }) {

            @Override
            public byte[] getBody() {
                String body = "grant_type=password&username=" + user.getEmail() + "&password=" + user.getPassword() + "&scope=openid profile email";
                return body.getBytes();
            }

            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                params.put("Authorization", "Basic cm9fY2xpZW50OnBhc3N3b3Jk");
                return params;
            }

           /* @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("grant_type", "password");
                params.put("username", "sumit.sharma@gmail.com");
                params.put("password", "Abcd1234");
                params.put("scope", "openid profile email");
                return params;
            }*/
        };

        // Adding request to request queue
        // jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(2000000,
                5,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        if (Utils.isConnected((Activity) context)) {
            AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
        } else {
            webServiceListener.isNetworkAvailable(false);
        }

    }

    public void getOrders(String userId, final WebServiceListener webServiceListener) {

        // Tag used to cancel the request
        String tag_json_obj = "getOrders";
        String url = URL_MY_ORDERS + "1";

        //final ProgressDialog pDialog = new ProgressDialog(context);
        pDialog.setMessage("Loading Orders...");
        pDialog.show();

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                url, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("", response.toString());
                        pDialog.hide();
                        if (response != null) {
                            OrderListResponse orderListResponse = new Gson().fromJson(response.toString(), OrderListResponse.class);
                            if (orderListResponse != null) {
                                webServiceListener.onSuccess(REQUEST_CODE_GET_MY_ORDERS, 200, orderListResponse);
                            } else {
                                orderListResponse = new OrderListResponse();
                                orderListResponse.setMessage(context.getResources().getString(R.string.internal_server_error));
                                webServiceListener.onFailure(REQUEST_CODE_GET_MY_ORDERS, 0, orderListResponse);
                            }
                        } else {
                            OrderListResponse orderListResponse = new OrderListResponse();
                            orderListResponse.setMessage(context.getResources().getString(R.string.internal_server_error));
                            webServiceListener.onFailure(REQUEST_CODE_GET_MY_ORDERS, 0, orderListResponse);
                        }


                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.hide();
                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        OrderListResponse orderListResponse = new Gson().fromJson(res, OrderListResponse.class);
                        if (orderListResponse != null) {
                            webServiceListener.onFailure(REQUEST_CODE_GET_MY_ORDERS, 200, orderListResponse);
                        } else {
                            orderListResponse = new OrderListResponse();
                            orderListResponse.setMessage(context.getResources().getString(R.string.internal_server_error));
                            webServiceListener.onFailure(REQUEST_CODE_GET_MY_ORDERS, 0, orderListResponse);
                        }

                    } catch (UnsupportedEncodingException e1) {
                        e1.printStackTrace();
                        OrderListResponse orderListResponse = new OrderListResponse();
                        orderListResponse.setMessage(context.getResources().getString(R.string.internal_server_error));
                        webServiceListener.onFailure(REQUEST_CODE_GET_MY_ORDERS, 0, orderListResponse);
                    }
                } else {
                    OrderListResponse orderListResponse = new OrderListResponse();
                    orderListResponse.setMessage(context.getResources().getString(R.string.internal_server_error));
                    webServiceListener.onFailure(REQUEST_CODE_GET_MY_ORDERS, 0, orderListResponse);
                }
            }
        }) {
        };

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(2000000, 5, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        if (Utils.isConnected((Activity) context)) {
            AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
        } else {
            webServiceListener.isNetworkAvailable(false);
        }

    }

    public void getColleges(WebServiceListener webServiceListener) {

        // Tag used to cancel the request
        String tag_json_obj = "loginUser";
        String url = URL_GET_CATEGORIES;

        //final ProgressDialog pDialog = new ProgressDialog(context);
        pDialog.setMessage("Logging in...");
        //pDialog.show();

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                url, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("", response.toString());
                        pDialog.hide();


                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.hide();
                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers, "utf-8"));


                    } catch (UnsupportedEncodingException e1) {
                        e1.printStackTrace();
                    }
                } else {

                }
            }
        }) {

            @Override
            public byte[] getBody() {
                String body = "grant_type=password&username=" + "" + "&password=" + "" + "&scope=openid profile email";
                return body.getBytes();
            }

            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                params.put("Authorization", "Basic cm9fY2xpZW50OnBhc3N3b3Jk");
                return params;
            }

           /* @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("grant_type", "password");
                params.put("username", "sumit.sharma@gmail.com");
                params.put("password", "Abcd1234");
                params.put("scope", "openid profile email");
                return params;
            }*/
        };

        // Adding request to request queue
        // jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(2000000, 5, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        if (Utils.isConnected((Activity) context)) {
            AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
        } else {
            webServiceListener.isNetworkAvailable(false);
        }

    }

    public void getCategories(final WebServiceListener webServiceListener) {

        // Tag used to cancel the request
        String tag_json_obj = "getOrders";
        String url = URL_GET_CATEGORIES;

        //final ProgressDialog pDialog = new ProgressDialog(context);
        pDialog.setMessage("Loading Categories..");
        pDialog.show();

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                url, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("", response.toString());
                        pDialog.hide();
                        CategoryResponse categoryResponse = new Gson().fromJson(response.toString(), CategoryResponse.class);
                        webServiceListener.onSuccess(REQUEST_CODE_GET_CATEGORIES, 200, categoryResponse);
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.hide();
                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data, HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        CategoryResponse categoryResponse = new Gson().fromJson(response.toString(), CategoryResponse.class);
                        webServiceListener.onSuccess(REQUEST_CODE_GET_CATEGORIES, 0, res);


                    } catch (UnsupportedEncodingException e1) {
                        e1.printStackTrace();
                        CategoryResponse categoryResponse = new Gson().fromJson(response.toString(), CategoryResponse.class);
                        webServiceListener.onSuccess(REQUEST_CODE_GET_CATEGORIES, 0, e1.getMessage());
                    }
                } else {
                    webServiceListener.onFailure(REQUEST_CODE_GET_CATEGORIES, 0, null);
                }
            }
        }) {

        };

        // Adding request to request queue
        // jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(2000000, 5, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        if (Utils.isConnected((Activity) context)) {
            AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
        } else {
            webServiceListener.isNetworkAvailable(false);
        }

    }

    public void getFoods(String categoryId, final WebServiceListener webServiceListener) {

        // Tag used to cancel the request
        String tag_json_obj = "getFoods";
        String url = URL_GET_FOODS + categoryId;

        //final ProgressDialog pDialog = new ProgressDialog(context);
        pDialog.setMessage("Loading Foods...");
        pDialog.show();

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                url, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("", response.toString());
                        pDialog.hide();
                        if (response != null) {
                            FoodListResponse foodListResponse = new Gson().fromJson(response.toString(), FoodListResponse.class);
                            webServiceListener.onSuccess(REQUEST_CODE_GET_FOODS, 200, foodListResponse);
                        } else {
                            FoodListResponse foodListResponse = new FoodListResponse();
                            foodListResponse.setMessage(context.getResources().getString(R.string.internal_server_error));
                            webServiceListener.onFailure(REQUEST_CODE_GET_FOODS, 0, foodListResponse);
                        }

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.hide();
                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data, HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        FoodListResponse foodListResponse = new Gson().fromJson(response.toString(), FoodListResponse.class);
                        webServiceListener.onSuccess(REQUEST_CODE_GET_FOODS, 0, foodListResponse);
                    } catch (UnsupportedEncodingException e1) {
                        e1.printStackTrace();
                        FoodListResponse foodListResponse = new FoodListResponse();
                        foodListResponse.setMessage(context.getResources().getString(R.string.internal_server_error));
                        webServiceListener.onFailure(REQUEST_CODE_GET_FOODS, 0, foodListResponse);
                    }
                } else {
                    FoodListResponse foodListResponse = new FoodListResponse();
                    foodListResponse.setMessage(context.getResources().getString(R.string.internal_server_error));
                    webServiceListener.onFailure(REQUEST_CODE_GET_FOODS, 0, foodListResponse);
                }
            }
        }) {

        };

        // Adding request to request queue
        // jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(2000000, 5, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        if (Utils.isConnected((Activity) context)) {
            AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
        } else {
            webServiceListener.isNetworkAvailable(false);
        }

    }

    public void orderFood(final String user_id, ArrayList<CartItem> cartItems, final WebServiceListener webServiceListener) {

        // Tag used to cancel the request
        String tag_json_obj = "orderFood";
        String url = URL_ORDER_FOOD;
        Gson gson = new GsonBuilder().create();
        final JsonArray jsonOrderArray = gson.toJsonTree(cartItems).getAsJsonArray();
        double amount = 0;
        for (CartItem cartItem : cartItems) {
            amount += Double.parseDouble(cartItem.getSubAmount());
        }
        //final ProgressDialog pDialog = new ProgressDialog(context);
        pDialog.setMessage("Placing Order...");
        pDialog.show();

        final double finalAmount = amount;
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                url, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("", response.toString());
                        pDialog.hide();
                        if (response != null) {
                            CreateOrderResponse createOrderResponse = new Gson().fromJson(response.toString(), CreateOrderResponse.class);
                            webServiceListener.onSuccess(REQUEST_CODE_GET_FOODS, 200, createOrderResponse);
                        } else {
                            CreateOrderResponse createOrderResponse = new CreateOrderResponse();
                            createOrderResponse.setMessage(context.getResources().getString(R.string.internal_server_error));
                            webServiceListener.onFailure(REQUEST_CODE_GET_FOODS, 0, createOrderResponse);
                        }

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.hide();
                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data, HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        Log.d("", "");
                        CreateOrderResponse createOrderResponse = new Gson().fromJson(response.toString(), CreateOrderResponse.class);
                        webServiceListener.onSuccess(REQUEST_CODE_GET_FOODS, 0, createOrderResponse);
                    } catch (UnsupportedEncodingException e1) {
                        e1.printStackTrace();
                        CreateOrderResponse createOrderResponse = new CreateOrderResponse();
                        createOrderResponse.setMessage(context.getResources().getString(R.string.internal_server_error));
                        webServiceListener.onFailure(REQUEST_CODE_GET_FOODS, 0, createOrderResponse);
                    }
                } else {
                    CreateOrderResponse createOrderResponse = new CreateOrderResponse();
                    createOrderResponse.setMessage(context.getResources().getString(R.string.internal_server_error));
                    webServiceListener.onFailure(REQUEST_CODE_GET_FOODS, 0, createOrderResponse);
                }
            }
        }) {

            @Override
            public byte[] getBody() {
                String body = "user_id=" + user_id + "&orders=" + jsonOrderArray.toString() + "&amount=" + String.valueOf(finalAmount);
                return body.getBytes();
            }

            @Override
            protected Map<String, String> getParams() {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("user_id", user_id);
                params.put("orders", jsonOrderArray.toString());
                params.put("amount", "" + finalAmount);
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded";
            }


            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                return headers;
            }
        };

        // Adding request to request queue
        // jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(2000000, 5, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        if (Utils.isConnected((Activity) context)) {
            AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
        } else {
            webServiceListener.isNetworkAvailable(false);
        }

    }

    public void cancelOrder(Orders order, final WebServiceListener webServiceListener) {

        // Tag used to cancel the request
        String tag_json_obj = "cancelOrder";
        String url = URL_CANCEL_ORDER + "email=" + order.getOrder_id();

        //final ProgressDialog pDialog = new ProgressDialog(context);
        pDialog.setMessage("Logging in...");
        pDialog.show();


        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                url, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("", response.toString());
                        pDialog.hide();
                        LoginResponse loginResponse = new Gson().fromJson(response.toString(), LoginResponse.class);
                        if (loginResponse.getStatus() != null && loginResponse.getStatus().equalsIgnoreCase("success")) {
                            webServiceListener.onSuccess(REQUEST_CODE_LOGIN, 0, loginResponse);
                        } else {
                            webServiceListener.onFailure(REQUEST_CODE_LOGIN, 0, loginResponse);
                        }

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.hide();
                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        LoginResponse loginResponse = new Gson().fromJson(res, LoginResponse.class);
                        webServiceListener.onFailure(REQUEST_CODE_LOGIN, 0, loginResponse);

                    } catch (UnsupportedEncodingException e1) {
                        e1.printStackTrace();
                        webServiceListener.onFailure(REQUEST_CODE_LOGIN, 0, null);
                    }
                } else {
                    webServiceListener.onFailure(REQUEST_CODE_LOGIN, 0, null);
                }
            }
        }) {

        };

        // Adding request to request queue
        // jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(2000, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        if (Utils.isConnected((Activity) context)) {
            AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
        } else {
            webServiceListener.isNetworkAvailable(false);
        }

    }
}
