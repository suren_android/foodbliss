package com.ey.foodbliss.adapters;

/**
 * Created by Surendar.V on 9/26/2017.
 */

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ey.foodbliss.R;
import com.ey.foodbliss.ResponseEntities.CategoryResponse;
import com.ey.foodbliss.fragments.MenuFragment;

import java.util.List;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.FoodMenuFolder> {

    private List<CategoryResponse.Response> categories;
    private Context context;
    MenuFragment menuFragment;

    public CategoryAdapter(MenuFragment menuFragment, Context context, List<CategoryResponse.Response> itemList) {
        this.categories = itemList;
        this.context = context;
        this.menuFragment = menuFragment;
    }

    @Override
    public FoodMenuFolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.food_menu_item, null);
        FoodMenuFolder rcv = new FoodMenuFolder(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(FoodMenuFolder holder, int position) {
        holder.foodName.setText(categories.get(position).getName());
        //holder.countryPhoto.setImageResource(categories.get(position).getPhoto());
    }

    @Override
    public int getItemCount() {
        return this.categories.size();
    }

    public class FoodMenuFolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView foodName;
        public ImageView foodPhoto;

        public FoodMenuFolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            foodName = (TextView) itemView.findViewById(R.id.tvFoodName);
            foodPhoto = (ImageView) itemView.findViewById(R.id.food_photo);
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            CategoryResponse.Response selectedFoodItem = categories.get(position);
            menuFragment.moveToFoodListFragment(selectedFoodItem);
        }
    }
}