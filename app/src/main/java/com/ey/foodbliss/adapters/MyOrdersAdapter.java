package com.ey.foodbliss.adapters;

/**
 * Created by Surendar.V on 9/26/2017.
 */

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.ey.foodbliss.R;
import com.ey.foodbliss.fragments.MyOrdersFragment;
import com.ey.foodbliss.model.Orders;

import java.util.ArrayList;

public class MyOrdersAdapter extends RecyclerView.Adapter<MyOrdersAdapter.FoodMenuFolder> {

    private ArrayList<Orders> orderItems;
    private Context context;
    MyOrdersFragment fragment;

    public MyOrdersAdapter(MyOrdersFragment fragment, Context context, ArrayList<Orders> orderItems) {
        this.orderItems = orderItems;
        this.context = context;
        this.fragment = fragment;
    }

    @Override
    public FoodMenuFolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.order_item, null);
        FoodMenuFolder rcv = new FoodMenuFolder(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(FoodMenuFolder holder, int position) {
        holder.foodName.setText(orderItems.get(position).getOrder_items());
        //holder.countryPhoto.setImageResource(foodItems.get(position).getPhoto());
    }

    @Override
    public int getItemCount() {
        return this.orderItems.size();
    }

    public class FoodMenuFolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView foodName;
        public ImageView foodPhoto;
        public AppCompatButton btnCancel;
        public CardView cvOrder;

        public FoodMenuFolder(View itemView) {
            super(itemView);
            //itemView.setOnClickListener(this);
            foodName = (TextView) itemView.findViewById(R.id.tvFoodName);
            foodPhoto = (ImageView) itemView.findViewById(R.id.food_photo);
            btnCancel = (AppCompatButton) itemView.findViewById(R.id.btnCancel);
            cvOrder = (CardView) itemView.findViewById(R.id.cvOrder);
            cvOrder.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            switch (view.getId()) {
                case R.id.cvOrder:
                    Orders orderItem = orderItems.get(position);
                    showOrderDialog(orderItem);
                    break;

                case R.id.btnCancel:
                    fragment.cancelOrder(orderItems.get(position));
                    break;

                default:
                    break;

            }
        }
    }

    private void showOrderDialog(Orders orderItem) {
        Dialog appCompatDialog = new Dialog(context, android.R.style.Theme_Translucent_NoTitleBar);
        appCompatDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        appCompatDialog.setContentView(R.layout.layout_order_detail);
        appCompatDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        appCompatDialog.show();
    }
}