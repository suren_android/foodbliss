package com.ey.foodbliss.adapters;

/**
 * Created by Surendar.V on 9/26/2017.
 */

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ey.foodbliss.R;
import com.ey.foodbliss.fragments.FoodListFragment;
import com.ey.foodbliss.model.Foods;

import java.util.ArrayList;
import java.util.List;

public class FoodListAdapter extends RecyclerView.Adapter<FoodListAdapter.FoodMenuFolder> {

    private List<Foods> foods;
    private Context context;
    FoodListFragment foodListFragment;

    public FoodListAdapter(FoodListFragment foodListFragment, Context context, ArrayList<Foods> foods) {
        this.foods = foods;
        this.context = context;
        this.foodListFragment = foodListFragment;
    }

    @Override
    public FoodMenuFolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.food_menu_item, null);
        FoodMenuFolder rcv = new FoodMenuFolder(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(FoodMenuFolder holder, int position) {
        holder.foodName.setText(foods.get(position).getName());
        //holder.countryPhoto.setImageResource(foods.get(position).getPhoto());
    }

    @Override
    public int getItemCount() {
        return this.foods.size();
    }

    public class FoodMenuFolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView foodName;
        public ImageView foodPhoto;

        public FoodMenuFolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            foodName = (TextView) itemView.findViewById(R.id.tvFoodName);
            foodPhoto = (ImageView) itemView.findViewById(R.id.food_photo);
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            Foods selectedFoodItem = foods.get(position);
            foodListFragment.moveToFoodDetailActivity(selectedFoodItem);
        }
    }
}