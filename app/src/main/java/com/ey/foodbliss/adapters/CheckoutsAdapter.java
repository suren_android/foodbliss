package com.ey.foodbliss.adapters;

/**
 * Created by Surendar.V on 9/26/2017.
 */

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ey.foodbliss.R;
import com.ey.foodbliss.database.AppDatabaseHelper;
import com.ey.foodbliss.fragments.CheckoutFragment;
import com.ey.foodbliss.model.CartItem;

import java.util.ArrayList;

public class CheckoutsAdapter extends RecyclerView.Adapter<CheckoutsAdapter.FoodMenuFolder> {

    private ArrayList<CartItem> cartItems;
    private Context context;
    CheckoutFragment fragment;
    AppDatabaseHelper appDatabaseHelper;

    public CheckoutsAdapter(CheckoutFragment fragment, Context context, ArrayList<CartItem> cartItems) {
        this.cartItems = cartItems;
        this.context = context;
        this.fragment = fragment;
        appDatabaseHelper = new AppDatabaseHelper(context);
    }

    @Override
    public FoodMenuFolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.cart_item, null);
        FoodMenuFolder rcv = new FoodMenuFolder(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(FoodMenuFolder holder, int position) {
        CartItem cartItem = cartItems.get(position);
        holder.tvFoodName.setText(cartItem.getName());
        holder.tvQuantity.setText(cartItem.getQuantity() + " Nos");
        holder.tvSubAmount.setText("Rs." + (int) Double.parseDouble(cartItem.getSubAmount()) + "/-");
        //holder.countryPhoto.setImageResource(foodItems.get(position).getPhoto());
    }

    @Override
    public int getItemCount() {
        return this.cartItems.size();
    }

    public class FoodMenuFolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView tvFoodName, tvQuantity, tvSubAmount;
        public ImageView foodPhoto, ivDelete;

        public FoodMenuFolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            tvFoodName = (TextView) itemView.findViewById(R.id.tvFoodName);
            tvQuantity = (TextView) itemView.findViewById(R.id.tvQuantity);
            tvSubAmount = (TextView) itemView.findViewById(R.id.tvSubAmount);
            foodPhoto = (ImageView) itemView.findViewById(R.id.food_photo);
            ivDelete = (ImageView) itemView.findViewById(R.id.ivDelete);
            ivDelete.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (view.getId() == R.id.ivDelete) {
                long result = appDatabaseHelper.removeFromCart(cartItems.get(getAdapterPosition()));
                cartItems.remove(getAdapterPosition());
                notifyDataSetChanged();
                if (fragment.isCartIsEmpty(cartItems)) {
                    fragment.onCartEmpty(true);
                }
            } else {
                int position = getAdapterPosition();
                CartItem cartItem = cartItems.get(position);
            }
        }
    }

}