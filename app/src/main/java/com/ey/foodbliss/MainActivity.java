package com.ey.foodbliss;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.Toast;

import com.ey.foodbliss.ResponseEntities.LoginResponse;
import com.ey.foodbliss.fragments.CheckoutFragment;
import com.ey.foodbliss.fragments.FoodListFragment;
import com.ey.foodbliss.fragments.LoginFragment;
import com.ey.foodbliss.fragments.MenuFragment;
import com.ey.foodbliss.fragments.MyOrdersFragment;
import com.ey.foodbliss.fragments.SignUpFragment;
import com.ey.foodbliss.fragments.SuggestionFragment;
import com.ey.foodbliss.listeners.SocialLoginListener;
import com.ey.foodbliss.model.FoodItem;
import com.ey.foodbliss.utils.Preferences;
import com.ey.foodbliss.utils.Utils;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, GoogleApiClient.OnConnectionFailedListener, View.OnClickListener, FragmentManager.OnBackStackChangedListener {

    private static final String TAG = "SignInActivity";
    private static final int RC_SIGN_IN = 9001;

    private GoogleApiClient mGoogleApiClient;
    private CallbackManager callbackManager;
    private FacebookCallback<LoginResult> mLoginFacebookCallback;
    private LoginManager loginManager;
    FragmentTransaction fragmentTransaction;
    FragmentManager fragmentManager;

    /*Fragment index*/
    public static final int INDEX_HOME_FRAGMENT = 1;
    public static final int INDEX_LOGIN_FRAGMENT = 2;
    public static final int INDEX_SIGNUP_FRAGMENT = 3;
    public static final int INDEX_MY_ORDER_FRAGMENT = 4;
    public static final int INDEX_CHECKOUT_FRAGMENT = 5;
    public static final int INDEX_FEEDBACK_FRAGMENT = 6;
    public static final int INDEX_SHARE_FRAGMENT = 7;

    public static final int INDEX_LOGOUT_FRAGMENT = 8;
    public static final int INDEX_FOOD_LIST_FRAGMENT = 9;
    private SocialLoginListener socialLoginListener;
    HashMap<String, Fragment> fragments = new HashMap<String, Fragment>();
    private int lastIndex;
    private NavigationView navigationView;

    boolean isLoginned = false;
    Preferences preferences;
    private AlertDialog.Builder logoutConfirmDialog, closeAppDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        preferences = new Preferences(this);
        initViews();
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentManager.addOnBackStackChangedListener(this);
        Utils.getFacebokKeyHash(this);
        initialiseGoogleClient();
        initialiseFacebookLogin();
        setFragment(INDEX_HOME_FRAGMENT, null);
        setNavigationItemSelected(INDEX_HOME_FRAGMENT);
    }

    private void initViews() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("tool title");
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                Toast.makeText(getApplicationContext(), "" + String.valueOf(findViewById(R.id.flFramentContainer).getVisibility()), Toast.LENGTH_SHORT).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView = (NavigationView) findViewById(R.id.nav_view);

        navigationView.setNavigationItemSelectedListener(this);

        isLoginned = preferences.isLoginned();
        changeMenuOnLogin(isLoginned);
        initLogoutConfirmDialog();
        initCloseAppDialog();
    }

    private void initLogoutConfirmDialog() {
        logoutConfirmDialog = new AlertDialog.Builder(this);
        logoutConfirmDialog.setMessage(getResources().getString(R.string.logout_confirm_message));
        logoutConfirmDialog.setPositiveButton(getResources().getString(R.string.logout), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                logout();
            }
        });
        logoutConfirmDialog.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
    }

    private void initCloseAppDialog() {
        closeAppDialog = new AlertDialog.Builder(this);
        closeAppDialog.setMessage(getResources().getString(R.string.close_app_message));
        closeAppDialog.setPositiveButton(getResources().getString(R.string.exit), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();
            }
        });
        closeAppDialog.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
    }

    private void initialiseFacebookLogin() {
        FacebookSdk.sdkInitialize(this);
        callbackManager = CallbackManager.Factory.create();
        loginManager = LoginManager.getInstance();

        mLoginFacebookCallback =
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        // App code
                        GraphRequest request = GraphRequest.newMeRequest(
                                AccessToken.getCurrentAccessToken(),
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(
                                            JSONObject object,
                                            GraphResponse response) {
                                        // Application code
                                        Log.v("LoginActivity", response.toString());

                                        try {
                                            //and fill them here like so.
                                            String str_facebook_id = object.getString("id");
                                            String str_email_id = object.getString("email");
                                            String str_name = object.getString("name");
                                            socialLoginListener.onFacebookLoginComplete(true, response.toString());

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                            socialLoginListener.onFacebookLoginComplete(false, response.toString());
                                        }

                                    }
                                });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id,name,email,gender");
                        request.setParameters(parameters);
                        request.executeAsync();

                    }

                    @Override
                    public void onCancel() {
                        // App code
                        Log.d("", "");
                        socialLoginListener.onFacebookLoginComplete(false, "cancel");
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                        Log.d("", "");
                        socialLoginListener.onFacebookLoginComplete(false, exception.getMessage());
                    }
                }

        ;


        loginManager.registerCallback(callbackManager, mLoginFacebookCallback);
    }

    public void loginWithFacebook(SocialLoginListener socialLoginListener) {
        this.socialLoginListener = socialLoginListener;
        AccessToken accessToken = AccessToken.getCurrentAccessToken();

        if (accessToken == null)
            loginManager.logInWithReadPermissions(this, Arrays.asList("public_profile", "email"));

    }

    public void logoutWithFacebook() {
        FacebookSdk.sdkInitialize(getApplicationContext());
        LoginManager loginManager = LoginManager.getInstance();
        if (loginManager != null)
            loginManager.logOut();
    }


    private void initialiseGoogleClient() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        Button signInButton = findViewById(R.id.sign_in_button);
        //signInButton.setSize(SignInButton.SIZE_STANDARD);
        signInButton.setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {
       /* DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            Fragment currentFragment = getCurrentFragment();
            if (currentFragment != null && currentFragment instanceof MenuFragment) {
                ((MenuFragment) currentFragment).moveMenuOneStepBack();
            } else {
                fragmentManager.popBackStack();
            }
        }*/
       /* if (fragmentManager.getBackStackEntryCount() > 1) {
            Fragment currentFragment = getCurrentFragment();
            fragments.put(currentFragment.getTag(), currentFragment);
            fragmentManager.popBackStack();
        } else {
            super.onBackPressed();
        }*/
        Fragment currentFragment = getCurrentFragment();
        if (currentFragment instanceof MenuFragment) {
            showCloseAppDialog();
        } else {
            getSupportFragmentManager().popBackStack();
            //setFragment(INDEX_HOME_FRAGMENT, null);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_open_cart) {
            if (preferences.isLoginned()) {
                setFragment(INDEX_CHECKOUT_FRAGMENT, null);
            } else {
                showLogoutConfirmDialog();
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            setFragment(INDEX_HOME_FRAGMENT, null);
        } else if (id == R.id.nav_login) {
            setFragment(INDEX_LOGIN_FRAGMENT, null);
        } else if (id == R.id.nav_orders) {
            setFragment(INDEX_MY_ORDER_FRAGMENT, null);
        } else if (id == R.id.nav_cart) {
            setFragment(INDEX_CHECKOUT_FRAGMENT, null);
        } else if (id == R.id.nav_logout) {
            setFragment(INDEX_LOGOUT_FRAGMENT, null);
        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_feedback) {
            setFragment(INDEX_FEEDBACK_FRAGMENT, null);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private List<FoodItem> getAllItemList() {

        List<FoodItem> allItems = new ArrayList<FoodItem>();
        allItems.add(new FoodItem());
        allItems.add(new FoodItem());
        allItems.add(new FoodItem());
        allItems.add(new FoodItem());
        allItems.add(new FoodItem());
        allItems.add(new FoodItem());
        allItems.add(new FoodItem());
        allItems.add(new FoodItem());
        allItems.add(new FoodItem());

        return allItems;
    }

    @Override
    public void onStart() {
        super.onStart();

        OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(mGoogleApiClient);
        if (opr.isDone()) {
            // If the user's cached credentials are valid, the OptionalPendingResult will be "done"
            // and the GoogleSignInResult will be available instantly.
            Log.d(TAG, "Got cached sign-in");
            GoogleSignInResult result = opr.get();
            handleSignInResult(result);
        } else {
            // If the user has not previously signed in on this device or the sign-in has expired,
            // this asynchronous branch will attempt to sign in the user silently.  Cross-device
            // single sign-on will occur in this branch.
            opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                @Override
                public void onResult(GoogleSignInResult googleSignInResult) {
                    handleSignInResult(googleSignInResult);
                }
            });
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    // [START onActivityResult]
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }
    // [END onActivityResult]

    // [START handleSignInResult]
    private void handleSignInResult(GoogleSignInResult result) {
        Log.d(TAG, "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            updateUI(true);
        } else {
            // Signed out, show unauthenticated UI.
            updateUI(false);
        }
    }

    private void updateUI(boolean b) {
    }
    // [END handleSignInResult]

    // [START signIn]
    private void signWithGoogle() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }
    // [END signIn]

    // [START signOut]
    private void signOutFromGoogle() {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
            if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
                Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                        new ResultCallback<Status>() {
                            @Override
                            public void onResult(Status status) {
                                // [START_EXCLUDE]
                                // [END_EXCLUDE]
                            }
                        });
            }
        }
    }
    // [END signOut]

    // [START revokeAccess]
    private void revokeAccess() {
        Auth.GoogleSignInApi.revokeAccess(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                    }
                });
    }
    // [END revokeAccess]

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        // An unresolvable error has occurred and Google APIs (including Sign-In) will not
        // be available.
        Log.d(TAG, "onConnectionFailed:" + connectionResult);
    }

    @Override
    protected void onStop() {
        super.onStop();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sign_in_button:

                break;
        }
    }

    public void setFragment(int index, Object object) {
        if (lastIndex != index) {
            lastIndex = index;
            Fragment newFragment = null;
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
            String fragmentTag = "";
            switch (index) {
                case INDEX_HOME_FRAGMENT:
                    fragmentTag = "HomeFragment";
                    if (fragments.containsKey(fragmentTag)) {
                        newFragment = fragments.get(fragmentTag);
                        //Fragment currentFragment = getCurrentFragment();
                        //fragmentTransaction.hide(currentFragment);
                        fragmentTransaction.replace(R.id.flFramentContainer, newFragment, fragmentTag);
                        fragmentTransaction.addToBackStack("");
                        fragmentTransaction.commit();
                    } else {
                        newFragment = MenuFragment.newInstance();
                        fragmentTransaction.replace(R.id.flFramentContainer, newFragment, fragmentTag).addToBackStack("").commit();
                        fragments.put(fragmentTag, newFragment);
                    }
                    fragments.clear();
                    break;

                case INDEX_LOGIN_FRAGMENT:
                    fragmentTag = "LoginFragment";
                    if (fragments.containsKey(fragmentTag)) {
                        newFragment = fragments.get(fragmentTag);
                        //Fragment currentFragment = getCurrentFragment();
                        //fragmentTransaction.hide(currentFragment);
                        fragmentTransaction.replace(R.id.flFramentContainer, newFragment, fragmentTag);
                        fragmentTransaction.commit();
                    } else {
                        newFragment = LoginFragment.newInstance();
                        fragmentTransaction.replace(R.id.flFramentContainer, newFragment, fragmentTag).commit();
                        fragments.put(fragmentTag, newFragment);
                    }
                    break;

                case INDEX_SIGNUP_FRAGMENT:
                    fragmentTag = "SignUpFragment";
                    if (fragments.containsKey(fragmentTag)) {
                        newFragment = fragments.get(fragmentTag);
                        //Fragment currentFragment = getCurrentFragment();
                        //fragmentTransaction.hide(currentFragment);
                        fragmentTransaction.replace(R.id.flFramentContainer, newFragment, fragmentTag).commit();
//                        fragmentTransaction.commit();
                    } else {
                        newFragment = SignUpFragment.newInstance();
                        fragmentTransaction.replace(R.id.flFramentContainer, newFragment, fragmentTag).commit();
                        fragments.put(fragmentTag, newFragment);
                    }
                    break;

                case INDEX_MY_ORDER_FRAGMENT:
                    fragmentTag = "MyOrdersFragment";
                    if (fragments.containsKey(fragmentTag)) {
                        newFragment = fragments.get(fragmentTag);
                        //Fragment currentFragment = getCurrentFragment();
                        //fragmentTransaction.hide(currentFragment);
                        fragmentTransaction.replace(R.id.flFramentContainer, newFragment, fragmentTag);
                        fragmentTransaction.commit();
                    } else {
                        newFragment = MyOrdersFragment.newInstance();
                        fragmentTransaction.replace(R.id.flFramentContainer, newFragment, fragmentTag).commit();
                        fragments.put(fragmentTag, newFragment);
                    }
                    break;

                case INDEX_CHECKOUT_FRAGMENT:
                    fragmentTag = "CheckoutFragment";
                    if (fragments.containsKey(fragmentTag)) {
                        newFragment = fragments.get(fragmentTag);
                        //Fragment currentFragment = getCurrentFragment();
                        //fragmentTransaction.hide(currentFragment);
                        fragmentTransaction.replace(R.id.flFramentContainer, newFragment, fragmentTag);
                        fragmentTransaction.commit();
                    } else {
                        newFragment = CheckoutFragment.newInstance();
                        fragmentTransaction.replace(R.id.flFramentContainer, newFragment, fragmentTag).commit();
                        fragments.put(fragmentTag, newFragment);
                    }
                    break;

                case INDEX_FOOD_LIST_FRAGMENT:
                    newFragment = FoodListFragment.newInstance((String) object);
                    fragments.put(fragmentTag, newFragment);
                    fragmentTransaction.add(R.id.flFramentContainer, newFragment, fragmentTag).addToBackStack(fragmentTag).commit();
                    break;

                case INDEX_LOGOUT_FRAGMENT:
                    showLogoutConfirmDialog();
                    break;

                case INDEX_FEEDBACK_FRAGMENT:
                    fragmentTag = "SuggestionFragment";
                    if (fragments.containsKey(fragmentTag)) {
                        newFragment = fragments.get(fragmentTag);
                        //Fragment currentFragment = getCurrentFragment();
                        //fragmentTransaction.hide(currentFragment);
                        fragmentTransaction.replace(R.id.flFramentContainer, newFragment, fragmentTag);
                        fragmentTransaction.commit();
                    } else {
                        newFragment = SuggestionFragment.newInstance();
                        fragmentTransaction.replace(R.id.flFramentContainer, newFragment, fragmentTag).commit();
                        fragments.put(fragmentTag, newFragment);
                    }
                    break;

                default:
                    break;
            }
        } else {
            Utils.showToast(this, "same index");
        }
        setNavigationItemSelected(lastIndex);
    }

    private void showLogoutConfirmDialog() {
        logoutConfirmDialog.show();
    }

    private void showCloseAppDialog() {
        closeAppDialog.show();
    }

    private Fragment getCurrentFragment() {
        return fragmentManager.findFragmentById(R.id.flFramentContainer);
    }

    private void logout() {
        logoutWithFacebook();
        signOutFromGoogle();
        LoginResponse.Response user = new LoginResponse.Response("", "", "", "", "", "", "", "", "");
        preferences.updateUser(user);
        preferences.setLoginned(false);
        changeMenuOnLogin(false);
        setFragment(INDEX_LOGIN_FRAGMENT, null);
    }

    @Override
    public void onBackStackChanged() {
        Fragment fragment = getCurrentFragment();
        if (fragment instanceof MenuFragment) {
            lastIndex = INDEX_HOME_FRAGMENT;
        } else if (fragment instanceof LoginFragment) {
            lastIndex = INDEX_LOGIN_FRAGMENT;
        } else if (fragment instanceof SignUpFragment) {
            lastIndex = INDEX_LOGIN_FRAGMENT;
        } else if (fragment instanceof MyOrdersFragment) {
            lastIndex = INDEX_MY_ORDER_FRAGMENT;
        }
        setNavigationItemSelected(lastIndex);
    }

    private void setNavigationItemSelected(int lastIndex) {
        navigationView.setNavigationItemSelectedListener(null);
        switch (lastIndex) {
            case INDEX_HOME_FRAGMENT:
                navigationView.getMenu().getItem(0).setChecked(true);
                break;

            case INDEX_LOGIN_FRAGMENT:
                navigationView.getMenu().getItem(1).setChecked(true);
                break;

            case INDEX_SIGNUP_FRAGMENT:
                navigationView.getMenu().getItem(1).setChecked(true);
                break;

            case INDEX_MY_ORDER_FRAGMENT:
                navigationView.getMenu().getItem(2).setChecked(true);
                break;

            case INDEX_FEEDBACK_FRAGMENT:

                break;

            default:
                break;
        }

        navigationView.setNavigationItemSelectedListener(this);
    }

    public void changeMenuOnLogin(boolean isLoginned) {
        MenuItem loginMenuItem = navigationView.getMenu().getItem(1);
        MenuItem logoutMenuItem = navigationView.getMenu().getItem(3);
        MenuItem orderMenuItem = navigationView.getMenu().getItem(2);
        if (isLoginned) {
            loginMenuItem.setVisible(false);
            logoutMenuItem.setVisible(true);
            orderMenuItem.setVisible(true);
        } else {
            loginMenuItem.setVisible(true);
            logoutMenuItem.setVisible(false);
            orderMenuItem.setVisible(false);
        }
    }
}
