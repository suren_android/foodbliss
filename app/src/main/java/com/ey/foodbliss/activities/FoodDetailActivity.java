package com.ey.foodbliss.activities;

import android.app.Dialog;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.NumberPicker;

import com.ey.foodbliss.R;
import com.ey.foodbliss.database.AppDatabaseHelper;
import com.ey.foodbliss.model.CartItem;
import com.ey.foodbliss.model.Foods;
import com.ey.foodbliss.utils.Preferences;
import com.ey.foodbliss.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;


public class FoodDetailActivity extends AppCompatActivity {

    Foods food;
    AppBarLayout appBarLayout;
    AppCompatTextView tvFoodId, tvFoodName, tvContent, tvPrice;
    AppDatabaseHelper appDatabaseHelper;
    com.ey.foodbliss.utils.Preferences preferences;
    int quantity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
        preferences = new Preferences(this);
        initViews();
        appDatabaseHelper = new AppDatabaseHelper(this);
        food = (Foods) getIntent().getExtras().getSerializable("food");
        setTitle(food.getName());
        setData();
    }

    private void setData() {
        tvFoodId.setText(food.getFood_id());
        tvFoodName.setText(food.getName());
        tvContent.setText(food.getContents());
        tvPrice.setText("Rs." + food.getPrice() + "/-");
    }

    private void initViews() {
        tvFoodId = (AppCompatTextView) findViewById(R.id.tvFoodId);
        tvFoodName = (AppCompatTextView) findViewById(R.id.tvFoodName);
        tvContent = (AppCompatTextView) findViewById(R.id.tvContent);
        tvPrice = (AppCompatTextView) findViewById(R.id.tvPrice);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.food_detail_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_add_cart) {
            if (preferences.isLoginned()) {
                showQuantityDialog();
            } else {

            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void showQuantityDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_number_picker);
        final NumberPicker np = (NumberPicker) dialog.findViewById(R.id.npQuantity);
        np.setMinValue(1);
        np.setMaxValue(10);
        np.setWrapSelectorWheel(true);
        np.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                //Display the newly selected number from picker

            }
        });
        AppCompatButton btnQuantityOk = (AppCompatButton) dialog.findViewById(R.id.btnQuantityOk);
        btnQuantityOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                quantity = np.getValue();
                dialog.dismiss();
                long result = addToCart();
                if (result >= 0) {
                    Utils.showAlert(FoodDetailActivity.this, food.getName() + "(" + quantity + ") added to the cart");
                }
            }
        });
        dialog.show();
    }

    public long addToCart() {
        long result;
        CartItem cartItem = new CartItem();
        cartItem.setUserId(preferences.getUser().getUser_id());
        cartItem.setCartId("" + System.currentTimeMillis());
        cartItem.setId(food.getFood_id());
        cartItem.setName(food.getName());
        cartItem.setPrice(food.getPrice());
        cartItem.setQuantity("" + quantity);
        cartItem.setSubAmount("" + (quantity * (Double.parseDouble(food.getPrice()))));
        JSONObject jsonFood = new JSONObject();
        result = appDatabaseHelper.addToCart(cartItem);

        return result;

    }
}
