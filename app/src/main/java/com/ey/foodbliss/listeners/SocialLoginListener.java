package com.ey.foodbliss.listeners;

/**
 * Created by Surendar.V on 10/6/2017.
 */

public interface SocialLoginListener {
    public void onGoogleLoginComplete();

    public void onFacebookLoginComplete(boolean success, String response);
}
